import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { WordpressProvider } from '../../providers/wordpress/wordpress';
import { LoadingController } from 'ionic-angular';
import { EventiDettaglioPage } from '../eventi-dettaglio/eventi-dettaglio';

@IonicPage()
@Component({
  selector: 'page-eventi',
  templateUrl: 'eventi.html',
})
export class EventiPage {

  listanews =null;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              private wpProv: WordpressProvider,
              public loadCtrl: LoadingController) {
  
  }

  displayData(item): string{
    return this.wpProv.estraiDataCustom(item);
  }

  ionViewDidLoad() {
    this.getlistaeventi();
  }

  prv(){
    console.log(this.listanews);
  }

  getlistaeventi() {
  let loader = this.loadCtrl.create({
    content: "caricamento...",
  });
  
    loader.present().then(() => {
      this.wpProv.getEventi().then(res =>{
        this.listanews = res.events;
        console.log(res.events);
        loader.dismiss();
      }).catch(err =>{
        console.log("errore: " + err);
        loader.dismiss();
    });
    });
  }

  itemTapped(event, item) {
    this.navCtrl.push(EventiDettaglioPage, {
      item: item
    });
  }
}