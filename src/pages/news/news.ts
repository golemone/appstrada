import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { WordpressProvider } from '../../providers/wordpress/wordpress';
import { LoadingController } from 'ionic-angular';
import { NewsDettaglioPage } from '../news-dettaglio/news-dettaglio';

@IonicPage()
@Component({
  selector: 'page-news',
  templateUrl: 'news.html',
})
export class NewsPage {

	listanews =null;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              private wpProv: WordpressProvider,
              public loadCtrl: LoadingController) {
  
  }

  displayDataC(item): string{
    return this.wpProv.estraiDataCustom(item);
  }

  ionViewDidLoad() {
    this.getlistaeventi();
  }

  prv(){
    console.log(this.listanews);
  }

  getlistaeventi() {
  let loader = this.loadCtrl.create({
    content: "caricamento...",
  });
  
    loader.present().then(() => {
      this.wpProv.getPosts().then(res =>{
        this.listanews = res;
        console.log(res);
        loader.dismiss();
      }).catch(err =>{
        console.log("errore: " + err);
        loader.dismiss();
    });
    });
  }

  itemTapped(event, item) {
    this.navCtrl.push(NewsDettaglioPage, {
      item: item
    });
  }
}
