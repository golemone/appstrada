import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewsDettaglioPage } from './news-dettaglio';

@NgModule({
  declarations: [
    NewsDettaglioPage,
  ],
  imports: [
    IonicPageModule.forChild(NewsDettaglioPage),
  ],
})
export class NewsDettaglioPageModule {}
