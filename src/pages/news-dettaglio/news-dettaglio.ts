import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-news-dettaglio',
  templateUrl: 'news-dettaglio.html',
})
export class NewsDettaglioPage {

selectedItem: any;
datapubblicazione: string


  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.selectedItem = navParams.get('item');
    this.datapubblicazione = this.estraidata(navParams.get('item'));
  }

  ionViewDidLoad() {
    console.log(this.selectedItem);
    this.estraidata(this.navParams.get('item'));
  }

  estraidata(item: any){
    var options = {'weekday': 'long', 'month': 'long', 'day': '2-digit', 'hour': '2-digit', 'minute':'2-digit'};
    // referenza delle opzioni data 
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toLocaleString
    var date = new Date(item.date).toLocaleString('it-IT', options);;
    //console.log(date);  // Wed Jan 01 2014 13:28:56 GMT-1000 (Hawaiian Standard Time) 
    return date
  }

}
