import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-eventi-dettaglio',
  templateUrl: 'eventi-dettaglio.html',
})
export class EventiDettaglioPage {

selectedItem: any;
urlImg: string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.selectedItem = navParams.get('item');
  	this.urlImg = navParams.get('item').image.url
  }

  ionViewDidLoad() {
    //anche niente
  }

  letmesee(){
  	console.log(this.selectedItem.image.url);
  }


}
