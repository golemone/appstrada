import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventiDettaglioPage } from './eventi-dettaglio';

@NgModule({
  declarations: [
    EventiDettaglioPage,
  ],
  imports: [
    IonicPageModule.forChild(EventiDettaglioPage),
  ],
})
export class EventiDettaglioPageModule {}
