import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { NewsPage } from '../pages/news/news';
import { NewsDettaglioPage } from '../pages/news-dettaglio/news-dettaglio';
import { EventiPage } from '../pages/eventi/eventi';
import { EventiDettaglioPage } from '../pages/eventi-dettaglio/eventi-dettaglio';
  
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AutenticazioneProvider } from '../providers/autenticazione/autenticazione';
import { WordpressProvider } from '../providers/wordpress/wordpress';

import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { ImpostazioniProvider } from '../providers/impostazioni/impostazioni';

@NgModule({
  declarations: [
    MyApp,
    NewsPage,
    NewsDettaglioPage,
    EventiPage,
    EventiDettaglioPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    NewsPage,
    NewsDettaglioPage,
    EventiPage,
    EventiDettaglioPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AutenticazioneProvider,
    WordpressProvider,
    ImpostazioniProvider
  ]
})
export class AppModule {}
