import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class WordpressProvider {

	rootUrl = "https://www.csoalastrada.it/wp-json/"

	eventEndpoint = "tribe/events/v1/events"
	postsEndpoint = "wp/v2/posts"
	mediaEndpoint = "wp/v2/media/"


	constructor(public http: HttpClient) {
	}

	// metodo di utilità per le date solo per il formato JSON. esempio -> 2018-11-20T12:18:16
	estraiData(item){
	    var options = {'weekday': 'long', 'month': 'long', 'day': '2-digit'}; //  per aggiungere orario bisogna aggiungere alle opzioni 'hour': '2-digit', 'minute':'2-digit'
	    // referenza delle opzioni data 
	    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toLocaleString
	    var date = new Date(item.date).toLocaleString('it-IT', options);;
	    //console.log(date);  // Wed Jan 01 2014 13:28:56 GMT-1000 (Hawaiian Standard Time) 
	    return date
  	}

  	// metodo di utilità per le date degli eventi che si differenziano rispetto alle date standart erogate dalle api di Wordpress
  	estraiDataCustom(item:string){
  		var dataJSON = item.replace(" ", "T");
	    var options = {'weekday': 'long', 'month': 'long', 'day': '2-digit','hour': '2-digit', 'minute':'2-digit'}; //  per aggiungere orario bisogna aggiungere alle opzioni 'hour': '2-digit', 'minute':'2-digit'
	    // referenza delle opzioni data 
	    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toLocaleString
	    var date = new Date(dataJSON).toLocaleString('it-IT', options);;
	    //console.log(date);  // Wed Jan 01 2014 13:28:56 GMT-1000 (Hawaiian Standard Time) 
	    return date	
  	}

	getMedia(id: string): Promise<any> {
		return new Promise((resolve,reject) => {
			this.http.get(this.rootUrl+this.mediaEndpoint + id ).subscribe(data =>{
				resolve(data);
			}, err => {
				reject(JSON.stringify(err));
			})
		})
	}

	getPosts(): Promise<any> {
		return new Promise((resolve, reject)=>{
			this.http.get(this.rootUrl+this.postsEndpoint).subscribe(data => {
				resolve(data);
			}, err => {
				reject(JSON.stringify(err));
			})
		})
	}


	getEventi(): Promise<any>{
		return new Promise((resolve, reject)=>{
		this.http.get(this.rootUrl+this.eventEndpoint).subscribe(data =>{
			resolve(data);
		}, err=>{
			reject(JSON.stringify(err));
		});
	})
	}
}
